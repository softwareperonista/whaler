# English translations for com.github.sdv43.whaler package.
# Copyright (C) 2022 THE com.github.sdv43.whaler'S COPYRIGHT HOLDER
# This file is distributed under the same license as the com.github.sdv43.whaler package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: com.github.sdv43.whaler\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-04 08:24+0300\n"
"PO-Revision-Date: 2022-04-09 14:17+0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/com.github.sdv43.whaler.desktop.in:5
msgid "Manage your Docker containers"
msgstr "Manage your Docker containers"

#: data/com.github.sdv43.whaler.desktop.in:11
msgid "Docker;Virtualizaton;Development;"
msgstr "Docker;Virtualizaton;Development;"

#: data/com.github.sdv43.whaler.appdata.xml.in:5
msgid "Docker Container Management"
msgstr "Docker Container Management"

#: data/com.github.sdv43.whaler.appdata.xml.in:10
msgid ""
"Whaler provides basic functionality for managing Docker containers. The app "
"can start and stop both standalone containers and docker-compose "
"applications. Also, it supports viewing container logs."
msgstr ""
"Whaler provides basic functionality for managing Docker containers. The app "
"can start and stop both standalone containers and docker-compose "
"applications. Also, it supports viewing container logs."

#: data/com.github.sdv43.whaler.appdata.xml.in:15
msgid ""
"The solution is perfect for those who are looking for a simple tool to "
"perform some basic actions. For the app to run correctly, make sure that "
"Docker is installed on your system."
msgstr ""
"The solution is perfect for those who are looking for a simple tool to "
"perform some basic actions. For the app to run correctly, make sure that "
"Docker is installed on your system."

#: src/Utils/Sorting/SortingName.vala:12
#: src/Widgets/Utils/ContainerInfoDialog.vala:52
msgid "Name"
msgstr "Name"

#: src/Utils/Sorting/SortingType.vala:10
msgid "Type"
msgstr "Type"

#: src/Widgets/Utils/SettingsDialog.vala:11
#: src/Widgets/Utils/ContainerInfoDialog.vala:13
msgid "Close"
msgstr "Close"

#: src/Widgets/Utils/SettingsDialog.vala:26
msgid "Settings"
msgstr "Settings"

#: src/Widgets/Utils/SettingsDialog.vala:41
msgid "API socket path:"
msgstr "API socket path:"

#: src/Widgets/Utils/SettingsDialog.vala:104
msgid "Check connection"
msgstr "Check connection"

#: src/Widgets/Utils/SettingsDialog.vala:124
msgid "Success"
msgstr "Success"

#: src/Widgets/Utils/SettingsDialog.vala:124
msgid "Error"
msgstr "Error"

#: src/Widgets/Utils/SettingsDialog.vala:141
#, c-format
msgid "Incorrect socket path \"%s\""
msgstr "Incorrect socket path \"%s\""

#: src/Widgets/Utils/DockerContainerStatusLabel.vala:11
msgid "Running"
msgstr "Running"

#: src/Widgets/Utils/DockerContainerStatusLabel.vala:16
msgid "Paused"
msgstr "Paused"

#: src/Widgets/Utils/DockerContainerStatusLabel.vala:21
msgid "Stopped"
msgstr "Stopped"

#: src/Widgets/Utils/DockerContainerStatusLabel.vala:26
msgid "Unknown"
msgstr "Unknown"

#: src/Widgets/Utils/ContainerInfoDialog.vala:53
msgid "Image"
msgstr "Image"

#: src/Widgets/Utils/ContainerInfoDialog.vala:54
msgid "Status"
msgstr "Status"

#: src/Widgets/Utils/ContainerInfoDialog.vala:57
msgid "Ports"
msgstr "Ports"

#: src/Widgets/Utils/ContainerInfoDialog.vala:61
msgid "Binds"
msgstr "Binds"

#: src/Widgets/Utils/ContainerInfoDialog.vala:65
msgid "Env"
msgstr "Env"

#: src/Widgets/HeaderBar.vala:15
msgid "Back"
msgstr "Back"

#: src/Widgets/HeaderBar.vala:40
msgid "Update docker container list"
msgstr "Update docker container list"

#: src/Widgets/HeaderBar.vala:87 src/Widgets/ScreenError.vala:56
msgid "Open settings"
msgstr "Open settings"

#: src/Widgets/ScreenError.vala:33
msgid ""
"It looks like Docker requires root rights to use it. Thus, the application "
"cannot connect to Docker Engine API. Find out how to run docker without root "
"rights in <a href=\"https://docs.docker.com/engine/install/linux-postinstall/"
"\">Docker Manuals</a>, otherwise the application cannot work correctly. Or "
"check your socket path to Docker API in Settings."
msgstr ""
"It looks like Docker requires root rights to use it. Thus, the application "
"cannot connect to Docker Engine API. Find out how to run docker without root "
"rights in <a href=\"https://docs.docker.com/engine/install/linux-postinstall/"
"\">Docker Manuals</a>, otherwise the application cannot work correctly. Or "
"check your socket path to Docker API in Settings."

#: src/Widgets/ScreenError.vala:42
msgid ""
"It looks like Docker is not installed on your system. To find out how to "
"install it, see <a href=\"https://docs.docker.com/engine/install/\">Docker "
"Manuals</a>. Or check your socket path to Docker API in Settings."
msgstr ""
"It looks like Docker is not installed on your system. To find out how to "
"install it, see <a href=\"https://docs.docker.com/engine/install/\">Docker "
"Manuals</a>. Or check your socket path to Docker API in Settings."

#: src/Widgets/ScreenError.vala:50
msgid "The app cannot connect to Docker API"
msgstr "The app cannot connect to Docker API"

#: src/Widgets/Screens/DockerContainer/Log.vala:39
msgid "Autoscroll"
msgstr "Autoscroll"

#: src/Widgets/Screens/DockerContainer/Log.vala:51
msgid "Enable autoscroll to bottom border"
msgstr "Enable autoscroll to bottom border"

#: src/Widgets/Screens/DockerContainer/TopBarActions.vala:19
msgid "Start"
msgstr "Start"

#: src/Widgets/Screens/DockerContainer/TopBarActions.vala:23
msgid "Stop"
msgstr "Stop"

#: src/Widgets/Screens/DockerContainer/TopBarActions.vala:25
msgid "Unpause"
msgstr "Unpause"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:56
msgid "Pause"
msgstr "Pause"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:59
msgid "Container pause error"
msgstr "Container pause error"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:74
msgid "Restart"
msgstr "Restart"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:76
msgid "Container restart error"
msgstr "Container restart error"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:79
msgid "Restarting container"
msgstr "Restarting container"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:94
msgid "Remove"
msgstr "Remove"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:97
msgid "Do you really want to remove container?"
msgstr "Do you really want to remove container?"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:98
msgid "Yes, remove"
msgstr "Yes, remove"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:99
msgid "Cancel"
msgstr "Cancel"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:105
msgid "Container remove error"
msgstr "Container remove error"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:107
msgid "Removing container"
msgstr "Removing container"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:127
msgid "Info"
msgstr "Info"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:129
msgid "Cannot get information"
msgstr "The app cannot get container data"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:152
msgid "Container action error"
msgstr "Container action error"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:157
msgid "Container stop error"
msgstr "Container stop error"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:158
msgid "Stopping container"
msgstr "Stopping container"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:163
msgid "Container start error"
msgstr "Container start error"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:164
msgid "Starting container"
msgstr "Starting container"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:169
msgid "Container unpause error"
msgstr "Container unpause error"

#: src/Widgets/Screens/Main/ContainerCardActions.vala:174
msgid "Container state is unknown"
msgstr "Container state is unknown"

#: src/Widgets/Screens/Main/ContainersGrid.vala:81
msgid "No containers"
msgstr "No containers"

#: src/Widgets/Screens/Main/ContainersGridFilter.vala:53
msgid "Sort by:"
msgstr "Sort by:"
