using Utils.Constants;

[GtkTemplate(ui = "/com/github/sdv43/whaler/ui/HeaderBar.ui")]
class Widgets.HeaderBar : Gtk.HeaderBar {
    [GtkChild]
    private unowned Gtk.Button button_back;
    [GtkChild]
    private unowned Gtk.Button button_refresh;
    private State.Root state;

    public HeaderBar () {
        this.set_state_root ();
    }

    private void set_state_root () {
        this.state = State.Root.get_instance ();
        this.state.notify["button-back-visible"].connect (() => {
            this.button_back.visible = this.state.button_back_visible;
        });

        this.state.notify["active-screen"].connect (() => {
            this.button_refresh.sensitive = this.state.active_screen == ScreenMain.CODE
                                       || this.state.active_screen == ScreenError.CODE;
        });
    }

    [GtkCallback]
    private void on_button_back_clicked () {
        this.state.prev_screen ();
    }

    [GtkCallback]
    private void on_button_back_show () {
        this.button_back.visible = this.state.button_back_visible;
    }

    [GtkCallback]
    private void on_button_refresh_clicked () {
        this.button_refresh.get_style_context ().add_class ("refresh-animation");

            Timeout.add (600, () => {
                this.button_refresh.get_style_context ().remove_class ("refresh-animation");

                return false;
            }, Priority.LOW);

            this.state.containers_load.begin ((_, res) => {
                try {
                    this.state.containers_load.end (res);

                    if (this.state.active_screen == ScreenError.CODE) {
                        this.state.active_screen = ScreenMain.CODE;
                    }
                } catch (Docker.ApiClientError error) {
                    var error_widget = ScreenError.build_error_docker_not_avialable (
                        error is Docker.ApiClientError.ERROR_NO_ENTRY
                    );

                    ScreenManager.screen_error_show_widget (error_widget);
                }
            });
    }

    [GtkCallback]
    private void on_button_refresh_show () {
        this.button_refresh.sensitive = this.state.active_screen == ScreenMain.CODE
                                       || this.state.active_screen == ScreenError.CODE;
    }

    [GtkCallback]
    private void on_button_settings_clicked () {
        new Utils.SettingsDialog ();
    }
}
