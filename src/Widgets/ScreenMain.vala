using Widgets.Screens.Main;

[GtkTemplate(ui = "/com/github/sdv43/whaler/ui/ScreenMain.ui")]
class Widgets.ScreenMain : Gtk.Box {
    public const string CODE = "main";
    /* These variables are created because in their absence
     * GtkBuilder cannot find their classes.
     * */
    [GtkChild]
    private unowned ContainersGridFilter containers_grid_filter;
    [GtkChild]
    private unowned ContainersGrid containers_grid;

    public ScreenMain () {
        this.containers_grid.set_visible(true);
        this.containers_grid_filter.set_visible(true);
    }
}
