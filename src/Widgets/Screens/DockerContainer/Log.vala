using Utils.Constants;

[GtkTemplate(ui = "/com/github/sdv43/whaler/ui/Log.ui")]
class Widgets.Screens.Container.Log : Gtk.Overlay {
    /* This variable is created because in its absence
     * GtkBuilder cannot find their classes.
     * */
    [GtkChild]
    private unowned Widgets.Screens.Container.LogOutput log_output;
    [GtkChild]
    private unowned Gtk.EventBox event_box;
    [GtkChild]
    private unowned Gtk.Box box;
    [GtkChild]
    private unowned Gtk.Switch switcher;

    construct {
        log_output.set_visible (true);
        event_box.set_events (box.get_events () | Gdk.EventMask.ENTER_NOTIFY_MASK);

        var settings = new Settings (APP_ID);
        settings.bind ("screen-docker-container-autoscroll", switcher, "active", SettingsBindFlags.DEFAULT);

        var state_root = State.Root.get_instance ();
        state_root.screen_docker_container.is_auto_scroll_enable = switcher.active;
    }

    [GtkCallback]
    private bool on_enter_notify_event_event_box () {
        box.get_style_context ().add_class ("visible");
        return false;
    }

    [GtkCallback]
    private bool on_leave_notify_event_event_box () {
        box.get_style_context ().remove_class ("visible");
        return false;
    }

    [GtkCallback]
    private bool on_enter_notify_event_switcher () {
        box.get_style_context ().add_class ("visible");
        return false;
    }

    [GtkCallback]
    private bool on_leave_notify_event_switcher () {
        box.get_style_context ().remove_class ("visible");
        return false;
    }

    [GtkCallback]
    private void on_notify_activate_switch () {
        var state_root = State.Root.get_instance ();
        state_root.screen_docker_container.is_auto_scroll_enable = switcher.active;
    }
}
