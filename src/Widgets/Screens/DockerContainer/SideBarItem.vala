using Utils;

[GtkTemplate(ui = "/com/github/sdv43/whaler/ui/SideBarItem.ui")]
class Widgets.Screens.Container.SideBarItem : Gtk.ListBoxRow {
    [GtkChild]
    private unowned Gtk.Label container_name;
    [GtkChild]
    private unowned Gtk.Label container_image;
    public DockerContainer service;

    public SideBarItem (DockerContainer service) {
        this.service = service;

        container_name.set_label (service.name);
        container_image.set_label (service.image);
    }
}
