[GtkTemplate (ui = "/com/github/sdv43/whaler/ui/ContaintersGrid.ui")]
class Widgets.Screens.Main.ContainersGrid : Gtk.Stack {
    public const string CODE = "main";

    private signal void container_cards_updated();

    private State.Root state;
    private State.ScreenMain state_main;

    [GtkChild]
    private unowned Gtk.FlowBox flow_box;

    construct {
        this.state = State.Root.get_instance ();
        this.state_main = this.state.screen_main;

        this.container_cards_updated.connect (() => {
            this.flow_box.foreach ((child) => {
                this.flow_box.remove (child);
            });

            foreach (var container in this.state_main.containers_prepared) {
                this.flow_box.add (new ContainerCard (container));
            }

            this.flow_box.show_all ();
        });

        this.state_main.notify["containers-prepared"].connect (() => {
            if (this.state_main.containers_prepared.size > 0) {
                this.set_visible_child_name ("containers");

                if (this.state.active_screen == ContainersGrid.CODE) {
                    this.container_cards_updated ();
                }
            } else {
                this.set_visible_child_name ("no-containers");
            }
        });

        this.state.notify["active-screen"].connect (() => {
            if (this.state.active_screen == ContainersGrid.CODE) {
                this.container_cards_updated ();
            }
        });
    }

    [GtkCallback]

    private void on_flow_box_child_activated (Gtk.FlowBoxChild child) {
        this.state.screen_docker_container.container = this.state_main.containers_prepared[child.get_index ()];
        this.state.next_screen (Widgets.ScreenDockerContainer.CODE);
    }
}

